//Pascal's Triangle

object PascalTriangle {

    def main(args: Array[String]) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution
*/
        
        val sc = new java.util.Scanner (System.in);
        
        for(i <- 0 to (sc.nextInt - 1) )
    	
    	println( pascal(i,0)  )
    }
    
    
    def pascal(n:Int, k:Int, s1:String = ""):String={
    
    if(k <= n)
    	pascal(n,k+1,s1.concat( (factorial(n)/( factorial(k) * (factorial(n - k) ) ) ).toString ).concat(" ") )
	else
		s1
    }
  
  def factorial(n:Int):Int={
    
    if(n==0)
      1
    else
      n*factorial(n-1)
  }

}