package hackerranck.solutions

import scala.collection.mutable._
import java.util.Scanner

object FunctionOrNot extends App{
  
 val sc = new Scanner(System.in);

    val numTest = sc.nextInt

    for( i <- Range(0,numTest) ){
	
        var numPars = sc.nextInt
        val mutMap = Map.empty[Int,Int]
		
        for(j <- Range(0,numPars) ){

			var p1 = sc.nextInt
            var p2 = sc.nextInt
            
			mutMap.put( p1, p2 )
			
            }//numPars

            if( numPars == mutMap.size)
                println("YES")
            else
                println("NO")
        }//NumTest
  
}