//Super Digit
object SuperDigit {

    def main(args: Array[String]) {

         val sc = new java.util.Scanner (System.in)
         val line = sc.nextLine
         val data = line.split(" ")
         
         val firstSum = data(0).toList map(x=>x - '0') sum
         
         if(data(1).toInt % 10 == 0)
             println( calcSuperDigit( firstSum.toString.toList map(x=>x - '0')  )   )
         else 
             println( calcSuperDigit( (firstSum * data(1).toInt ).toString.toList map(x=>x - '0') ) )
         
    }
    
     def calcSuperDigit(lista:List[Int]):Int={
    
            if(lista.size==1)
              lista(0)
            else
              calcSuperDigit(lista.sum.toString.toList map(x=>x - '0'))
    
  }
}